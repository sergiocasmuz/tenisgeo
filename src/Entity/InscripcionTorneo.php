<?php

namespace App\Entity;

use App\Repository\InscripcionTorneoRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: InscripcionTorneoRepository::class)]
class InscripcionTorneo
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'inscripcionTorneos')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Jugador $jugador1 = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Torneo $torneo = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJugador1(): ?Jugador
    {
        return $this->jugador1;
    }

    public function setJugador1(?Jugador $jugador1): static
    {
        $this->jugador1 = $jugador1;

        return $this;
    }

    public function getTorneo(): ?Torneo
    {
        return $this->torneo;
    }

    public function setTorneo(?Torneo $torneo): static
    {
        $this->torneo = $torneo;

        return $this;
    }
}
