<?php

namespace App\Repository;

use App\Entity\InscripcionTorneo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<InscripcionTorneo>
 *
 * @method InscripcionTorneo|null find($id, $lockMode = null, $lockVersion = null)
 * @method InscripcionTorneo|null findOneBy(array $criteria, array $orderBy = null)
 * @method InscripcionTorneo[]    findAll()
 * @method InscripcionTorneo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InscripcionTorneoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InscripcionTorneo::class);
    }

    //    /**
    //     * @return InscripcionTorneo[] Returns an array of InscripcionTorneo objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('i.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?InscripcionTorneo
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
