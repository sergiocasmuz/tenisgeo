# Tenis Docker


## Empecemos

1. Si aún no lo has hecho, instala Docker Compose (v2.10+).
2. Ejecuta `docker compose build --no-cache` para construir imágenes nuevas desde cero.
3. Ejecuta `docker compose up --pull always -d --wait`  para iniciar el proyecto.
4. Abre `https://localhost` en tu navegador y [acepta el certificado TLS generado automáticamente.](https://stackoverflow.com/a/15076602/1352334)
5. Ejecuta `echo yes | docker exec -i tenisgeo-php-1 php bin/console doctrine:fixtures:load` para correr los fixtures, esto cargará jugadores en el sistema
