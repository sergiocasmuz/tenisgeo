<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class SimulacionesController extends AbstractController
{
    #[Route('/simulaciones', name: 'app_simulaciones')]
    public function index(): Response
    {
        return $this->render('simulaciones/index.html.twig', [
            'controller_name' => 'SimulacionesController',
        ]);
    }
}
