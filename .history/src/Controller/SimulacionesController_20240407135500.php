<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\HttpFoundation\JsonResponse;

class SimulacionesController extends AbstractController
{
    #[Route('/simulaciones', name: 'app_simulaciones')]
    public function index(): Response
    {
        return $this->render('simulaciones/index.html.twig', [
            'controller_name' => 'SimulacionesController',
        ]);
    }
}
