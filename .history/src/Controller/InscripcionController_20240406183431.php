<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class InscripcionController extends AbstractController
{
    #[Route('/inscripcion', name: 'app_inscripcion')]
    public function index(): Response
    {
        return $this->render('inscripcion/index.html.twig', [
            'controller_name' => 'InscripcionController',
        ]);
    }
}
