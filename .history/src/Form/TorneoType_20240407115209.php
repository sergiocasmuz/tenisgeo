<?php

namespace App\Form;

use App\Entity\Torneo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class TorneoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nombre')
            ->add('genero', ChoiceType::class, [
                'choices' => [
                    'Masculino' => true,
                    'Femenino' => false,
                ],
            ])
            ->add('cantidad', ChoiceType::class, [
                'choices' => [
                    '4 Jugadores' => 4,
                    '8 Jugadores' => 8,
                    '16 Jugadores' => 16,
                    '32 Jugadores' => 32,
                    '64 Jugadores' => 64,
                ],
            ])
            ->add('etapas')
        ;

        // Agregamos un evento para calcular automáticamente las etapas
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $torneo = $event->getData();
            $form = $event->getForm();

            if ($torneo instanceof Torneo && null !== $torneo->getCantidad()) {
                // Calculamos las etapas basadas en la cantidad de jugadores
                $cantidad = $torneo->getCantidad();
                $etapas = log($cantidad, 2);
                $etapas = ceil($etapas);

                // Añadimos las opciones de etapas al formulario
                $choices = [];
                for ($i = 1; $i <= $etapas; $i++) {
                    $choices[$i] = $i;
                }

                $form->add('etapas', ChoiceType::class, [
                    'choices' => array_flip($choices),
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Torneo::class,
        ]);
    }
}
