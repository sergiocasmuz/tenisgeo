<?php

namespace App\Form;

use App\Entity\Torneo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TorneoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nombre')
            ->add('genero', ChoiceType::class, [
                'choices' => [
                    'Masculino' => true,
                    'Femenino' => false,
                ],
            ])
            ->add('cantidad', ChoiceType::class, [
                'choices' => [
                    '4' => true,
                    '8' => false,
                    '16' => false,
                    '8' => false,
                    '8' => false,
                ],
            ])
            ->add('etapas')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Torneo::class,
        ]);
    }
}
