<?php

namespace App\Entity;

use App\Repository\PartidoRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PartidoRepository::class)]
class Partido
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Torneo $torneo = null;

    #[ORM\ManyToOne]
    private ?Jugador $jugador_1 = null;

    #[ORM\ManyToOne]
    private ?Jugador $jugador_2 = null;

    #[ORM\Column]
    private ?int $etapa = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTorneo(): ?Torneo
    {
        return $this->torneo;
    }

    public function setTorneo(?Torneo $torneo): static
    {
        $this->torneo = $torneo;

        return $this;
    }

    public function getJugador1(): ?Jugador
    {
        return $this->jugador_1;
    }

    public function setJugador1(?Jugador $jugador_1): static
    {
        $this->jugador_1 = $jugador_1;

        return $this;
    }

    public function getJugador2(): ?Jugador
    {
        return $this->jugador_2;
    }

    public function setJugador2(?Jugador $jugador_2): static
    {
        $this->jugador_2 = $jugador_2;

        return $this;
    }

    public function getEtapa(): ?int
    {
        return $this->etapa;
    }

    public function setEtapa(int $etapa): static
    {
        $this->etapa = $etapa;

        return $this;
    }
}
