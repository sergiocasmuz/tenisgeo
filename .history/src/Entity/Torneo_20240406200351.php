<?php

namespace App\Entity;

use App\Repository\TorneoRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TorneoRepository::class)]
class Torneo
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nombre = null;

    #[ORM\Column]
    private ?bool $genero = null;

    #[ORM\Column]
    private ?int $cantidad = null;

    #[ORM\Column]
    private ?int $etapas = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): static
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function isGenero(): ?bool
    {
        return $this->genero;
    }

    public function setGenero(bool $genero): static
    {
        $this->genero = $genero;

        return $this;
    }

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): static
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getEtapas(): ?int
    {
        return $this->etapas;
    }

    public function setEtapas(int $etapas): static
    {
        $this->etapas = $etapas;

        return $this;
    }
}
