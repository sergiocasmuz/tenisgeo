<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'Simular',
    description: 'Este command se encarga de realizar la simulacion con las ditintas estapas',
)]
class SimularCommand extends Command
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }


        use App\Service\SimularTorneo;

class SimularCommand extends Command
{
    private $simularTorneo;

    public function __construct(SimularTorneo $simularTorneo)
    {
        parent::__construct();
        $this->simularTorneo = $simularTorneo;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Accede a los argumentos y opciones según sea necesario
        $arg1 = $input->getArgument('arg1');
        $option1 = $input->getOption('option1');

        // Llama a los métodos de tu servicio SimularTorneo según lo necesites
        // Ejemplo: $this->simularTorneo->Etapa($torneo, $etapa);

        return Command::SUCCESS;
    }
}



        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}
