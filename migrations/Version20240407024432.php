<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240407024432 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE resultado ADD torneo_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE resultado ADD etapa INT DEFAULT NULL');
        $this->addSql('ALTER TABLE resultado ADD CONSTRAINT FK_B2ED91CA0139802 FOREIGN KEY (torneo_id) REFERENCES torneo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_B2ED91CA0139802 ON resultado (torneo_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE resultado DROP CONSTRAINT FK_B2ED91CA0139802');
        $this->addSql('DROP INDEX IDX_B2ED91CA0139802');
        $this->addSql('ALTER TABLE resultado DROP torneo_id');
        $this->addSql('ALTER TABLE resultado DROP etapa');
    }
}
