<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240406204832 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE partido_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE partido (id INT NOT NULL, torneo_id INT NOT NULL, jugador_1_id INT DEFAULT NULL, jugador_2_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4E79750BA0139802 ON partido (torneo_id)');
        $this->addSql('CREATE INDEX IDX_4E79750B4A87D322 ON partido (jugador_1_id)');
        $this->addSql('CREATE INDEX IDX_4E79750B58327CCC ON partido (jugador_2_id)');
        $this->addSql('ALTER TABLE partido ADD CONSTRAINT FK_4E79750BA0139802 FOREIGN KEY (torneo_id) REFERENCES torneo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE partido ADD CONSTRAINT FK_4E79750B4A87D322 FOREIGN KEY (jugador_1_id) REFERENCES jugador (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE partido ADD CONSTRAINT FK_4E79750B58327CCC FOREIGN KEY (jugador_2_id) REFERENCES jugador (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE partido_id_seq CASCADE');
        $this->addSql('ALTER TABLE partido DROP CONSTRAINT FK_4E79750BA0139802');
        $this->addSql('ALTER TABLE partido DROP CONSTRAINT FK_4E79750B4A87D322');
        $this->addSql('ALTER TABLE partido DROP CONSTRAINT FK_4E79750B58327CCC');
        $this->addSql('DROP TABLE partido');
    }
}
