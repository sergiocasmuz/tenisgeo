<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240406213028 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE inscripcion_torneo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE inscripcion_torneo (id INT NOT NULL, jugador1_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F319C6CF390198F4 ON inscripcion_torneo (jugador1_id)');
        $this->addSql('ALTER TABLE inscripcion_torneo ADD CONSTRAINT FK_F319C6CF390198F4 FOREIGN KEY (jugador1_id) REFERENCES jugador (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE inscripcion_torneo_id_seq CASCADE');
        $this->addSql('ALTER TABLE inscripcion_torneo DROP CONSTRAINT FK_F319C6CF390198F4');
        $this->addSql('DROP TABLE inscripcion_torneo');
    }
}
