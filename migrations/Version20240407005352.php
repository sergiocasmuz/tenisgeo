<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240407005352 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE resultado_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE resultado (id INT NOT NULL, partido_id INT DEFAULT NULL, ganador_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B2ED91C11856EB4 ON resultado (partido_id)');
        $this->addSql('CREATE INDEX IDX_B2ED91CA338CEA5 ON resultado (ganador_id)');
        $this->addSql('ALTER TABLE resultado ADD CONSTRAINT FK_B2ED91C11856EB4 FOREIGN KEY (partido_id) REFERENCES partido (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE resultado ADD CONSTRAINT FK_B2ED91CA338CEA5 FOREIGN KEY (ganador_id) REFERENCES jugador (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE resultado_id_seq CASCADE');
        $this->addSql('ALTER TABLE resultado DROP CONSTRAINT FK_B2ED91C11856EB4');
        $this->addSql('ALTER TABLE resultado DROP CONSTRAINT FK_B2ED91CA338CEA5');
        $this->addSql('DROP TABLE resultado');
    }
}
